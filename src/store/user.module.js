import JWTService from '../services/jwt.service';
import AuthService from '../services/auth.service';
import ApiService from '../services/api.service';

// user Module
// namespace: user


export const state = {
  user: null,
  account: null,
};

export const getters = {
  /**
   * Checks if the user is authenticated.
   */
  isAuthenticated(state) {
    return state.user != null && state.account != null;
  },
};

export const mutations = {
  /**
   * Modify state's user data.
   * @param user User data.
   */
  setUser(state, user) {
    state.user = user;
  },

  /**
   * Modify state's account data.
   * @param user Account data.
   */
  setAccount(state, account) {
    state.account = account;
  },
};

export const actions = {
  /**
   * Logs in the users with the given credentials.
   * JWT Token is stored if the login is successful.
   * @param credentials User credentials.
   */
  async login({ commit }, { username, password }) {
    const { token, data, error } = await AuthService.login(username, password);
    if (!error) {
      JWTService.storeToken(token);
      AuthService.storeUserId(data._id);
      commit('setUser', {
        id: data._id,
        username: data.username,
        email: data.email,
      });
      return true;
    }

    return false;
  },

  async register(context, { username, email, password }) {
    const { error } = await AuthService.register(username, email, password);
    return !error;
  },

  async validateAccount(context, { id, token }) {
    const { error } = await AuthService.validateAccount(id, token);
    return !error;
  },

  async createAccount(context, {
    id, firstname, lastname, biography, token,
  }) {
    const { error } = await AuthService.createAccount(id, firstname, lastname, biography, token);
    return !error;
  },

  /**
   * Fetches the connected user data and stores it in the module's state.
   */
  async fetchUser({ commit }) {
    const id = AuthService.getUserId();
    if (id) {
      const { error, data } = await AuthService.getUser(id);
      if (!error) {
        commit('setUser', {
          id: data._id,
          username: data.username,
          email: data.email,
        });
        return true;
      }
    }

    commit('setUser', null);
    return false;
  },

  async fetchAccount({ commit }) {
    const id = AuthService.getUserId();
    if (id) {
      const { error, data } = await AuthService.getAccount(id);
      if (!error && data.length === 1) {
        commit('setAccount', data[0]);
        return true;
      }
    }

    commit('setAccount', null);
    return false;
  },

  /*
* @param data {Object} contains some fields used to modify account
*/
  async modifyAccount({ commit }, _data) {
    const { error, data } = await AuthService.setAccount(_data.id, _data);
    if (!error) {
      commit('setAccount', data);
      return true;
    }
    return false;
  },

  async favoriteProject({ dispatch, state }, project) {
    await ApiService.post(`/accounts/${state.account._id}/favourites/${project}`, null, true);
    await dispatch('fetchAccount');
  },

  /**
   * Check if the user is authenticated.
   * Updates the state's user data accordingly.
   */
  async checkAuth({ dispatch, getters }) {
    if (JWTService.getToken() && AuthService.getUserId()) {
      await Promise.all([dispatch('fetchUser'), dispatch('fetchAccount')]);
      if (getters.isAuthenticated) {
        return true;
      }
    }

    dispatch('clearAuth');
    return false;
  },

  /**
   * Clears the authentication data.
   */
  clearAuth({ commit }) {
    JWTService.removeToken();
    AuthService.removeUserId();
    commit('setUser', null);
    commit('setAccount', null);
  },
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
