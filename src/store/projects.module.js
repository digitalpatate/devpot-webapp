import ApiService from '../services/api.service';

// Projects Module
// namespace: projects

const initialState = {
  projects: null,
  loading: false,
};

export const state = { ...initialState };

export const actions = {

  /**
   * Fetches a project list asynchonrously. Also manages the loading state.
   * @param prevDevlog The previous devlog (optional).
   */
  async fetchProjects({ commit }, prevProjects = null) {
    commit('setLoading', true);

    let projects = prevProjects;

    if (!projects) {
      const { status, data } = await ApiService.get('/projects');
      if (status === 200 && !data.error) {
        projects = data.data;
      }
    }
    commit('setProjects', projects);
    commit('setLoading', false);
  },
};

export const mutations = {
  /**
   * Modifies the project list in the module's state.
   * @param projects The new project list.
   */
  setProjects(state, projects) {
    state.projects = projects;
  },

  /**
   * Modifies the devlog in the module's state.
   * @param {boolean} loading The new loading state.
   */
  setLoading(state, loading) {
    state.loading = loading;
  },
};

export const getters = {
  /**
   * Retrieves the current project list.
   */
  projects(state) {
    return state.projects;
  },

  /**
   * Retrieves the loading state.
   */
  loading(state) {
    return state.loading;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
