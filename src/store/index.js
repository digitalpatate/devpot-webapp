import Vue from 'vue';
import Vuex from 'vuex';

import user from './user.module';
import project from './project.module';
import projects from './projects.module';
import devlog from './devlog.module';
import devlogs from './devlogs.module';
import images from './images.module';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    user,
    project,
    projects,
    devlog,
    devlogs,
    images,
  },
});
