import ApiService from '../services/api.service';

// Project Module
// namespace: project

const initialState = {
  project: null,
  devlogs: [],
  loading: false,
};

export const state = { ...initialState };

export const actions = {
  /**
   * Fetches a project and its associated deblogs.
   * Also manages the loading state. Asynchronous.
   * @param {string} idProject ID of the project to fetch.
   */
  async fetch({ commit, dispatch }, { idProject, owner }) {
    commit('setLoading', true);
    await Promise.all([
      dispatch('fetchProject', idProject),
      dispatch('fetchDevlogs', { idProject, owner }),
    ]).then(() => {
      commit('setLoading', false);
    });
  },

  /**
   * Fetches a single project. Doesn't manage the loading state. Asynchronous
   * @param {string} idProject
   * @param prevProject The previous project (optional)
   */
  async fetchProject({ commit }, idProject, prevProject = null) {
    let project = prevProject;
    if (!project) {
      const { status, data } = await ApiService.get(`/projects/${idProject}`);
      if (status === 200 && !data.error) {
        project = data.data;
      }
    }
    commit('setProject', project);
  },

  /**
   * Fetches the devlog list of a project.
   * Doesn't manage the loading state. Asynchronous.
   * @param {string} idProject ID of the project.
   */
  async fetchDevlogs({ commit }, { idProject, owner }) {
    const { status, data } = await ApiService.get(
      `/devlogs?project=${idProject}&published=true${owner ? '&published=false' : ''}`,
    );

    let devlogs = [];
    if (status === 200 && !data.error) {
      devlogs = data.data;
    }
    commit('setDevlogs', devlogs);
  },

  async createProject({ commit }, _data) {
    const formData = new FormData();
    formData.append('title', _data.title);
    formData.append('description', _data.description);
    formData.append('file', _data.file);

    const { status, data } = await ApiService.post('/projects', formData, true, { 'Content-Type': 'multipart/form-data' });
    let project = null;
    if (status === 200 && !data.error) {
      project = data.data;
    } else if (data.message.errors != null) {
      const [firstError] = Object.values(data.message.errors);
      throw new Error(firstError.message);
    } else {
      throw new Error(data.message);
    }
    commit('setProject', project);
    return project._id;
  },

  async editProject({ commit }, _data) {
    const formData = new FormData();
    formData.append('title', _data.title);
    formData.append('description', _data.description);
    formData.append('file', _data.file);

    const { status, data } = await ApiService.put(`/projects/${_data._id}`, formData, true, { 'Content-Type': 'multipart/form-data' });
    let project = null;
    if (status === 200 && !data.error) {
      project = data.data;
    } else if (data.message.errors != null) {
      const [firstError] = Object.values(data.message.errors);
      throw new Error(firstError.message);
    } else {
      throw new Error(data.message);
    }
    commit('setProject', project);
    return project._id;
  },

};

export const mutations = {
  /**
   * Modifies the project in the module's state.
   * @param project The new project.
   */
  setProject(state, project) {
    state.project = project;
  },

  setProjectDescription(state, description) {
    state.project.description = description;
  },

  /**
   * Modifies the devlog list in the module's state.
   * @param devlogs The new devlog list.
   */
  setDevlogs(state, devlogs) {
    state.devlogs = devlogs;
  },

  /**
   * Modifies the loading state in the module's state.
   * @param loading The new loading state.
   */
  setLoading(state, loading) {
    state.loading = loading;
  },
};

export const getters = {
  /**
   * Retrieves the project.
   */
  project(state) {
    return state.project;
  },

  /**
   * Retrieves the devlog list.
   */
  devlogs(state) {
    return state.devlogs;
  },

  /**
   * Retrieves the loading state.
   */
  loading(state) {
    return state.loading;
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
