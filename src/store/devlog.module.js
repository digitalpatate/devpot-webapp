import ApiService from '../services/api.service';

// Devlog Module
// namespace: devlog

const initialState = {
  devlog: null,
  loading: false,
  imagesPaths: [],
};

export const state = { ...initialState };

export const actions = {

  /**
  * Fetches a single devlog asynchonrously. Also manages the loading state.
  * @param {string} idDevlog ID of the devlog to fetch.
  * @param prevDevlog The previous devlog (optional).
  */
  async fetchDevlog({ commit }, idDevlog, prevDevlog = null) {
    commit('setLoading', true);

    let devlog = prevDevlog;

    if (!devlog) {
      const { status, data } = await ApiService.get(`/devlogs/${idDevlog}`);
      if (status === 200 && !data.error) {
        devlog = data.data;
      }
    }
    commit('setDevlog', devlog);
    commit('setLoading', false);
  },

  /**
  * Create a new devlog asynchonrously.
  */
  async createDevlog({ commit }, devlog) {
    const { status, data } = await ApiService.post('/devlogs', devlog, true);
    let _devlog = null;
    if (status === 200 && !data.error) {
      _devlog = data.data;
    } else {
      throw new Error(`error ${status}\n${data.message}`);
    }
    commit('setDevlog', _devlog);
    return _devlog._id;
  },


  /**
  * edit an existing devlog
  */
  async editDevlog({ commit }, devlog) {
    const { status, data } = await ApiService.put(`/devlogs/${devlog.devlogId}`, devlog, true);
    let _devlog = null;
    if (status === 200 && !data.error) {
      _devlog = data.data;
    } else {
      throw new Error(`error ${status}\n${data.message}`);
    }
    commit('setDevlog', _devlog);
    return _devlog._id;
  },


};

export const mutations = {
  /**
  * Modifies the devlog in the module's state.
  * @param devlog The new devlog.
  */
  setDevlog(state, devlog) {
    state.devlog = devlog;
  },

  /**
  * set devlog content
  */
  setDevlogContent(state, content) {
    if (state.devlog != null) {
      state.devlog.content = content;
    }
  },

  /**
  * Modifies the devlog in the module's state.
  * @param {boolean} loading The new loading state.
  */
  setLoading(state, loading) {
    state.loading = loading;
  },

  /**
  * add an image path to the state (to keep track of recent images)
  */
  addImagePath(state, imagePath) {
    state.imagesPaths.push(imagePath);
  },

};

export const getters = {
  /**
  * Retrieves the current devlog.
  */
  devlog(state) {
    return state.devlog;
  },

  /**
  * Retrieves the loading state.
  */
  loading(state) {
    return state.loading;
  },

  imagesPaths(state) {
    return state.imagesPaths;
  },

};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
