import ApiService from '../services/api.service';

// Devlog Module
// namespace: devlog

const initialState = {
  imagesPaths: [],
};

export const state = { ...initialState };

export const actions = {

  async uploadImage({ commit }, { uploadPath, file }) {
    const { status, data } = await ApiService.postFile(uploadPath, file, true);
    if (status === 200 && !data.error) {
      console.log(data.data.path);
      commit('addImagePath', `${process.env.VUE_APP_BACKEND_URL}/${data.data.path}`);
    } else {
      console.log(data);
    }
  },

};


export const mutations = {
  /**
  * add an image path to the state (to keep track of recent images)
  */
  addImagePath(state, imagePath) {
    state.imagesPaths.push(imagePath);
  },

};

export const getters = {

  imagesPaths(state) {
    return state.imagesPaths;
  },

};


export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
