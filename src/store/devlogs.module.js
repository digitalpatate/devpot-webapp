import ApiService from '../services/api.service';

// Devlogs Module
// namespace: devlogs

export const state = {
  devlogs: null,
  loading: false,
};

export const getters = {};

export const mutations = {
  /**
   * Modifies the devlog list in the module's state.
   * @param devlogs The new devlog list.
   */
  setDevlogs(state, devlogs) {
    state.devlogs = devlogs;
  },

  /**
   * Modifies the loading state of the module's state.
   * @param {boolean} loading The new loading state.
   */
  setLoading(state, loading) {
    state.loading = loading;
  },
};

export const actions = {
  /**
   * Fetches a devlog list asynchonrously. Also manages the loading state.
   * @param prevDevlogs The previous devlog (optional).
   */
  async fetchDevlogs({ commit }) {
    commit('setLoading', true);

    let devlogs = null;

    const { status, data } = await ApiService.get('/devlogs?populate=project');
    if (status === 200 && !data.error) {
      devlogs = data.data;
    }

    commit('setDevlogs', devlogs);
    commit('setLoading', false);
  },
};

export default {
  namespaced: true,
  state,
  actions,
  mutations,
  getters,
};
