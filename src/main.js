import Vue from 'vue';
// import VueShowdown from 'vue-showdown';
import VueMq from 'vue-mq';
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome';
import router from './router';
import store from './store';
import App from './App.vue';
import ApiService from './services/api.service';
import '@/assets/css/tailwind.css';
import '@/assets/css/markdown.css';


import Default from './layouts/Default.vue';
import Sidebar from './layouts/Sidebar.vue';

ApiService.init();

Vue.component('fa-icon', FontAwesomeIcon);
Vue.component('default-layout', Default);
Vue.component('sidebar-layout', Sidebar);

Vue.config.productionTip = false;

Vue.use(VueMq, {
  breakpoints: {
    sm: 768,
    md: 1024,
    lg: 1280,
    xl: Infinity,
  },
});

// In case we desire to read markdown into html
// Vue.use(VueShowdown, {
//   // set default flavor of showdown
//   flavor: 'github',
//   // set default options of showdown (will override the flavor options)
//   options: {
//     emoji: true,
//   },
// });

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app');
