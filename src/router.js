import Vue from 'vue';
import Router from 'vue-router';
import store from './store';
import JWTService from './services/jwt.service';

Vue.use(Router);

// middlware

// Checks if the user is connected or redirects to login.
function connected() {
  if (!JWTService.getToken()) {
    return 'login';
  }
  return '';
}

// Checks if the user is disconnected or redirects to profile.
function disconnected() {
  if (JWTService.getToken()) {
    return 'profile';
  }
  return '';
}

// TODO: Add a fallback route for error 404 for all other routes

const router = new Router({
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import('./views/Home.vue'),
    },
    {
      path: '/projects/:id',
      name: 'project',
      component: () => import('./views/Project.vue'),
    },
    {
      path: '/projects',
      name: 'projects',
      component: () => import('./views/Projects.vue'),
    },

    {
      path: '/devlogs',
      name: 'devlogs',
      component: () => import('./views/Devlogs.vue'),
    },
    {
      path: '/devlogs/:id',
      name: 'devlog',
      component: () => import('./views/Devlog.vue'),
    },

    // Dashboard routes
    {
      path: '/dashboard/profile',
      name: 'profile',
      meta: {
        layout: 'sidebar',
        middleware: [connected],
      },
      component: () => import('./views/dashboard/Profile.vue'),
    },
    {
      path: '/dashboard/settings',
      name: 'settings',
      meta: {
        layout: 'sidebar',
        middleware: [connected],
      },
      component: () => import('./views/dashboard/Settings.vue'),
    },
    {
      path: '/new_project',
      redirect: { name: 'new_project' },
    },
    {
      path: '/dashboard/new_project',
      name: 'new_project',
      meta: {
        layout: 'sidebar',
        middleware: [connected],
      },
      component: () => import('./views/NewProject.vue'),
    },
    {
      path: '/projects/:id/edit',
      redirect: { name: 'projectEditor' },
    },
    {
      path: '/dashboard/projects/:id/edit',
      name: 'projectEditor',
      meta: {
        layout: 'sidebar',
        middleware: [connected],
      },
      component: () => import('./views/ProjectEditor.vue'),
    },
    {
      path: '/projects/:id/new_devlog',
      redirect: { name: 'new_devlog' },
    },
    {
      path: '/dashboard/projects/:id/new_devlog',
      name: 'new_devlog',
      meta: {
        layout: 'sidebar',
        middleware: [connected],
      },
      component: () => import('./views/NewDevlog.vue'),
    },
    {
      path: '/devlogs/:id/edit',
      redirect: { name: 'devlogEditor' },
    },
    {
      path: '/dashboard/devlogs/:id/edit',
      name: 'devlogEditor',
      meta: {
        layout: 'sidebar',
        middleware: [connected],
      },
      component: () => import('./views/DevlogEditor.vue'),
    },
    // Auth routes
    {
      path: '/login',
      name: 'login',
      meta: {
        middleware: [disconnected],
      },
      component: () => import('./views/Login.vue'),
    },
    {
      path: '/register',
      name: 'register',
      meta: {
        middleware: [disconnected],
      },
      component: () => import('./views/Register.vue'),
    },
    {
      path: '/validate',
      name: 'validate',
      meta: {
        middleware: [disconnected],
      },
      component: () => import('./views/ValidateAccount.vue'),
    },
    {
      path: '/logout',
      name: 'logout',
      meta: {
        middleware: [connected],
      },
      beforeEnter: (to, from, next) => {
        store.dispatch('user/clearAuth');
        next('/login');
      },
    },
  ],
});

router.beforeEach((to, from, next) => {
  store.dispatch('user/checkAuth').then(() => {
    let redirected = false;
    if (to.meta.middleware) {
      to.meta.middleware.forEach((middleware) => {
        const redirect = middleware();
        if (redirect !== '') {
          if (from.name === redirect) {
            next(false);
          } else {
            next({ name: redirect });
          }
          redirected = true;
        }
      });
    }
    if (!redirected) {
      next();
    }
  });
});

export default router;
