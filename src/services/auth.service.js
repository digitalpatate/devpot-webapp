import ApiService from './api.service';
import JWTService from './jwt.service';

const USER_ID_KEY = 'userId';

const AuthService = {
  /**
   * Send a login request via API service.
   * @param {String} username User's username.
   * @param {String} password User's password.
   */
  async login(username, password) {
    const { data } = await ApiService.post('/auth/login', { username, password });
    return data;
  },
  /**
   * Send a register request via API service.
   * @param {String} username User's username.
   * @param {String} email User's email.
   * @param {String} password User's password.
   */
  async register(username, email, password) {
    const { data } = await ApiService.post(
      '/auth/register',
      {
        username, email, password, frontUrl: `${process.env.VUE_APP_WEBAPP_URL}/validate`,
      },
    );
    return data;
  },
  /**
   * Sends an account validation request via API service.
   * @param {*} id User ID.
   * @param {*} token Registration token.
   */
  async validateAccount(id, token) {
    const { data } = await ApiService.post(
      `/auth/users/${id}/tokens/validate`, { token },
    );

    return data;
  },

  async createAccount(user, firstname, lastname, biography, token) {
    JWTService.storeToken(token);
    const { data } = await ApiService.post(
      '/accounts', {
        user, firstname, lastname, biography,
      }, true,
    );
    JWTService.removeToken();

    return data;
  },

  /**
   * Fetches the informations of a user.
   * Require its the user to be logged in before.
   * @param {String} id User ID.
   */
  async getUser(id) {
    const { data } = await ApiService.get(`/auth/users/${id}`, true);
    return data;
  },

  /**
   * Fetches the informations of a user.
   * Require its the user to be logged in before.
   * @param {String} id User ID.
   */
  async getAccount(id) {
    const { data } = await ApiService.get(`/accounts/?user=${id}`, true);
    return data;
  },

  async setAccount(id, _data) {
    const { data } = await ApiService.put(`/accounts/${id}`, _data, true);
    return data;
  },

  /**
   * Saves the user id in the local storage.
   * @param id User ID.
   */
  storeUserId(id) {
    window.localStorage.setItem(USER_ID_KEY, id);
  },
  /**
   * Removes the user id from the local storage
   */
  removeUserId() {
    window.localStorage.removeItem(USER_ID_KEY);
  },
  /**
   * Retrieves the user id.
   * @return The user id or null it does not exist.
   */
  getUserId() {
    return window.localStorage.getItem(USER_ID_KEY) || null;
  },
};

export default AuthService;
