import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import FormData from 'form-data';
import JWTService from './jwt.service';

const API_ERROR = {
  status: 400,
  data: {
    error: true,
    data: null,
    message: 'API Service Error',
  },
};

/**
 * Helper class for backend API requests.
 */
const ApiService = {
  init() {
    Vue.use(VueAxios, axios);
    axios.defaults.baseURL = process.env.VUE_APP_BACKEND_URL;
    axios.defaults.headers = {
      'Access-Control-Allow-Origin': true,
    };
  },

  /**
   * Sends a get request to the backend api.
   * @param {string} path Path to the resource.
   */
  get(path, withAuth = false) {
    return axios.get(path, {
      headers: withAuth ? this.authorizationHeader() : {},
    }).catch(this.handleError);
  },

  /**
   * Sends a get request to the backend api.
   * @param {string} Path to the resource.
   * @param {Object} data Data to post.
   * @param {bool} withAuth if being authenticated is required
   * @param {key1: value1, ...} optional additional headers
   */
  post(path, data, withAuth = false, additionalHeaders = {}) {
    return axios.post(path, data, {
      headers: {
        ...(withAuth ? this.authorizationHeader() : {}),
        ...additionalHeaders,
      },
    }).catch(this.handleError);
  },

  postFile(path, file, withAuth = false) {
    const formData = new FormData();
    formData.append('file', file);
    return axios.post(path, formData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        ...(withAuth ? this.authorizationHeader() : {}),
      },
    }).catch(this.handleError);
  },

  /**
   * Sends a put request to the backend api.
   * @param {string} Path to the resource.
   * @param {Object} data Data to put.
   */
  put(path, data, withAuth = false) {
    return axios.put(path, data, {
      headers: withAuth ? this.authorizationHeader() : {},
    }).catch(this.handleError);
  },

  /**
   * Sends a delete request to the backend api.
   * @param {string} Path to the resource.
   */
  delete(path, withAuth = false) {
    return axios.delete(path, {
      headers: withAuth ? this.authorizationHeader() : {},
    }).catch(this.handleError);
  },

  handleError(error) {
    if (error.response) {
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx
      return {
        status: error.response.status,
        data: error.response.data,
      };
    }

    return API_ERROR;
  },

  /**
   * Creates an authorization header if ther is a token.
   * Returns an empty object otherwise.
   */
  authorizationHeader() {
    const token = JWTService.getToken();
    return token ? { Authorization: `Bearer ${token}` } : {};
  },
};

export default ApiService;
