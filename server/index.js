const express = require('express');
const history = require('connect-history-api-fallback');
// ^ middleware to redirect all URLs to index.html

const app = express();
const staticFileMiddleware = express.static('./public');

app.use(staticFileMiddleware);
app.use(history({
  disableDotRule: true,
  verbose: true,
}));
app.use(staticFileMiddleware);
// app.use(staticFileMiddleware) is included twice as per
// https://github.com/bripkens/connect-history-api-fallback/tree/master/examples/static-files-and-index-rewrite

app.listen(8080, () => {
  console.log('Server started on port 8080!');
});
