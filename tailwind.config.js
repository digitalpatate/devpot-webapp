module.exports = {
  theme: {
    extend: {
      colors: {
        blue: {
          dark: '#2C3E50',
          light: '#34495E',
          transparent: 'rgba(44, 62, 80, 0.6)',
        },
        cyan: {
          dark: '#2980B9',
          light: '#3498DB',
        },
        white: {
          base: 'rgba(255, 255, 255, 1.0)',
          transparent: 'rgba(255, 255, 255, 0.8)',
        },
      },
    },
  },
  variants: {},
  plugins: [],
};
