# Devpot Webapp

## Run the project
First install nodejs dependencies with:
```
npm install
```

Then you can run a local server with hot-reload feature:
```
npm run serve
```

The project needs a running backend and mongodb database. By default the backend url is 'http://localhost:3000' for development mode. You can use the docker-compose.yml at the repository root to run both localy (You can run `npm run seed` inside of the backend project to add temporary data to the database). See 'Environnement variables' section dor more information on how to use the backend url.

## Tests

The unit tests can be run with:
```
npm run test:unit
```

## Linter

To seed the lint errors:
```
npm run lint
```

## Production

Vue js require to build the project for production. This is done with the following command:
```
npm run build
```

This creates a dist folder (ignored by git) containing almost everything needed for deployment. This project uses the vue route in HTML5 history mode. This mode requires the server to rewrite all the url to /index.html for the router to work properly (the app can be used as a simple static website). A simple server.js file does just that. To run the project in production (on port 8080) simply use the following command:
```
npm start
```

## Environnement variables

The .env files let us add environnement variables to the project. The variables need the be prefixed with 'VUE_APP_' to be available in code. The files '.env.production' contains variables available for production and '.env.development' for development.

'VUE_APP_BACKEN_URL' contains the url to the backend api. So it can be used in code like this:
```
process.env.VUE_APP_BACKEND_URL
```
You can ealily append any path for the request you want to do:
```
`${process.env.VUE_APP_BACKEND_URL}/devlogs`
```

To edit the url you can add a file name '.env.devlopment.local' to override the value. Every .env file that finishes with '.local' is ignored by git.

More informations on this [here](https://cli.vuejs.org/guide/mode-and-env.html).

## Vuex store

[Vuex](https://vuex.vuejs.org) is a state management pattern and library that serves as a centralized store for all the components in a vuejs application.

In this application, vuex's store in seperated in multiple modules. Each module manage a part of the store with its own actions, mutations and getters.

> Note: See source code for more documentation on how to use the actions, mutations and getters.