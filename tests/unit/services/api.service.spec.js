import { assert } from 'chai';
import sinon from 'sinon';

import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import ApiService from '@/services/api.service';
import JWTService from '@/services/jwt.service';

describe('Api Service', () => {
  describe('init', () => {
    it('it can init vue to use axios', () => {
      const vueUseStub = sinon.spy(Vue, 'use');

      ApiService.init();

      assert.deepEqual(vueUseStub.args, [
        [VueAxios, axios],
      ]);
      assert.equal(axios.defaults.baseURL, process.env.VUE_APP_BACKEND_URL);

      vueUseStub.restore();
    });
  });

  describe('get', () => {
    let axiosGetStub;
    function stubAxiosGet(f) {
      axiosGetStub = sinon.stub(axios, 'get')
        .callsFake(f);
    }

    afterEach(() => {
      axiosGetStub.restore();
    });

    it('it sends a get data', async () => {
      const path = '/potato';
      const responseMock = { status: 200 };
      stubAxiosGet(async () => responseMock);

      const response = await ApiService.get(path);

      assert.deepEqual(axiosGetStub.args, [
        [path, { headers: {} }],
      ]);

      assert.equal(response, responseMock);
    });

    it('it responds correctly when there is an error with axios', async () => {
      const path = '/potato';
      const error = 'error';
      stubAxiosGet(async () => {
        throw error;
      });

      const response = await ApiService.get(path);

      assert.deepEqual(axiosGetStub.args, [
        [path, { headers: {} }],
      ]);

      assert.deepEqual(response, {
        status: 400,
        data: {
          error: true,
          data: null,
          message: 'API Service Error',
        },
      });
    });
  });

  describe('post', () => {
    const path = '/potato';
    const data = { username: 'jack' };

    it('it sends a post request', async () => {
      const authHeaderSpy = sinon.spy(ApiService, 'authorizationHeader');
      const postStub = sinon.stub(axios, 'post')
        .callsFake(async () => {});

      await ApiService.post(path, data);

      assert.deepEqual(postStub.args, [
        [path, data, { headers: {} }],
      ]);

      assert.isTrue(authHeaderSpy.notCalled);

      authHeaderSpy.restore();
      postStub.restore();
    });

    it('it can send a post request with an authorization header', async () => {
      const authHeader = { Authorization: 'Bearer abcd' };
      const authHeaderStub = sinon.stub(ApiService, 'authorizationHeader')
        .returns(authHeader);
      const postStub = sinon.stub(axios, 'post')
        .callsFake(async () => {});

      await ApiService.post(path, data, true);

      assert.deepEqual(postStub.args, [
        [path, data, { headers: authHeader }],
      ]);

      assert.isTrue(authHeaderStub.called);

      authHeaderStub.restore();
      postStub.restore();
    });

    it('it responds correctly when there is an error with axios', async () => {
      const postStub = sinon.stub(axios, 'post')
        .callsFake(async () => { throw new Error(); });

      const response = await ApiService.post(path, data);

      assert.deepEqual(response, {
        status: 400,
        data: {
          error: true,
          data: null,
          message: 'API Service Error',
        },
      });

      postStub.restore();
    });
  });

  describe('authorizationHeader', () => {
    let getTokenStub;

    afterEach(() => {
      getTokenStub.restore();
    });

    it('it returns an authorization header', () => {
      getTokenStub = sinon.stub(JWTService, 'getToken').returns('1');

      const header = ApiService.authorizationHeader();

      assert.isTrue(getTokenStub.calledOnce);

      assert.deepEqual(header, {
        Authorization: 'Bearer 1',
      });
    });

    it('it returns an empty header if not authenticated', () => {
      getTokenStub = sinon.stub(JWTService, 'getToken').returns(null);

      const header = ApiService.authorizationHeader();

      assert.isTrue(getTokenStub.calledOnce);

      assert.deepEqual(header, {});
    });
  });
});
