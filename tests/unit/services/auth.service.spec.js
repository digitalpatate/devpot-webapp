import { assert } from 'chai';
import sinon from 'sinon';

import AuthService from '@/services/auth.service';
import ApiService from '@/services/api.service';

describe('Authentication Service', () => {
  const USER_ID_KEY = 'userId';
  const token = 'a1b2c3d4e5';
  const userId = 'a1b2c3d4e5';
  const username = 'jack';
  const email = 'jack@potato';
  const password = '123';
  const data = { error: false };

  const localStorage = {
    getItem: () => {},
    setItem: () => {},
    removeItem: () => {},
  };

  before(() => {
    global.window = { localStorage };
  });

  after(() => {
    delete global.window;
  });

  describe('login', () => {
    it('it sends a login request', async () => {
      const postStub = sinon.stub(ApiService, 'post')
        .callsFake(async () => ({ data }));

      const response = await AuthService.login(username, password);

      assert.deepEqual(postStub.args, [
        ['/auth/login', { username, password }],
      ]);

      assert.equal(response, data);

      postStub.restore();
    });
  });

  describe('register', () => {
    it('it sends a register request', async () => {
      const postStub = sinon.stub(ApiService, 'post')
        .callsFake(async () => ({ data }));

      const response = await AuthService.register(username, email, password);

      assert.deepEqual(postStub.args, [
        ['/auth/register', {
          username,
          email,
          password,
          frontUrl: `${process.env.VUE_APP_WEBAPP_URL}/validate`,
        }],
      ]);

      assert.equal(response, data);
      postStub.restore();
    });
  });

  describe('validateAccount', () => {
    it('it sends a register request', async () => {
      const postStub = sinon.stub(ApiService, 'post')
        .callsFake(async () => ({ data }));

      const response = await AuthService.validateAccount(userId, token);

      assert.deepEqual(postStub.args, [
        [`/auth/users/${userId}/tokens/validate`, { token }],
      ]);

      assert.equal(response, data);

      postStub.restore();
    });
  });

  describe('getUser', () => {
    it("it sends a request to get the user's informations", async () => {
      const getStub = sinon.stub(ApiService, 'get')
        .callsFake(async () => ({ data }));

      const response = await AuthService.getUser(userId);

      assert.deepEqual(getStub.args, [
        [`/auth/users/${userId}`, true],
      ]);

      assert.equal(response, data);

      getStub.restore();
    });
  });

  describe('getuserId', () => {
    it('it retrieves the user id from the local storage', () => {
      const storageSpy = sinon
        .stub(window.localStorage, 'getItem')
        .returns(userId);

      const retrievedUserId = AuthService.getUserId();

      assert.deepEqual(storageSpy.args, [
        [USER_ID_KEY],
      ]);
      assert.equal(retrievedUserId, userId);

      storageSpy.restore();
    });


    it('it returns null if there is no user id', () => {
      const storageSpy = sinon
        .stub(window.localStorage, 'getItem')
        .returns(undefined);

      const retrievedUserId = AuthService.getUserId();

      assert.deepEqual(storageSpy.args, [
        [USER_ID_KEY],
      ]);
      assert.isNull(retrievedUserId);

      storageSpy.restore();
    });
  });

  describe('storeUserId', () => {
    it('it stores the user id in the local storage', () => {
      const storageSpy = sinon.spy(window.localStorage, 'setItem');

      AuthService.storeUserId(userId);

      assert.deepEqual(storageSpy.args, [
        [USER_ID_KEY, userId],
      ]);

      storageSpy.restore();
    });
  });

  describe('removeuserId', () => {
    it('it removes the user id from the local storage', () => {
      const storageSpy = sinon.spy(window.localStorage, 'removeItem');

      AuthService.removeUserId();

      assert.deepEqual(storageSpy.args, [
        [USER_ID_KEY],
      ]);

      storageSpy.restore();
    });
  });
});
