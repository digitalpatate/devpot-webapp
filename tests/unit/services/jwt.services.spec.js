import { assert } from 'chai';
import sinon from 'sinon';

import JWTService from '@/services/jwt.service';

describe('JWT Service', () => {
  const TOKEN_KEY = 'jwtToken';
  const token = 'POTATOESAREGOOD';
  const localStorage = {
    getItem: () => {},
    setItem: () => {},
    removeItem: () => {},
  };

  before(() => {
    global.window = { localStorage };
  });

  after(() => {
    delete global.window;
  });

  describe('getToken', () => {
    it('it retrieves the token from the local storage', () => {
      const storageSpy = sinon
        .stub(window.localStorage, 'getItem')
        .returns(token);

      const retrievedToken = JWTService.getToken();

      assert.deepEqual(storageSpy.args, [
        [TOKEN_KEY],
      ]);
      assert.equal(retrievedToken, token);

      storageSpy.restore();
    });


    it('it returns null if there is no token', () => {
      const storageSpy = sinon
        .stub(window.localStorage, 'getItem')
        .returns(undefined);

      const retrievedToken = JWTService.getToken();

      assert.deepEqual(storageSpy.args, [
        [TOKEN_KEY],
      ]);
      assert.isNull(retrievedToken);

      storageSpy.restore();
    });
  });

  describe('storeToken', () => {
    it('it stores a token in the local storage', () => {
      const storageSpy = sinon.spy(window.localStorage, 'setItem');

      JWTService.storeToken(token);

      assert.deepEqual(storageSpy.args, [
        [TOKEN_KEY, token],
      ]);

      storageSpy.restore();
    });
  });

  describe('removeToken', () => {
    it('it removes the token from the local storage', () => {
      const storageSpy = sinon.spy(window.localStorage, 'removeItem');

      JWTService.removeToken();

      assert.deepEqual(storageSpy.args, [
        [TOKEN_KEY],
      ]);

      storageSpy.restore();
    });
  });
});
