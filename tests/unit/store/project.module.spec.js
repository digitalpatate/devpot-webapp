import { assert } from 'chai';
import sinon from 'sinon';
import { mutations, actions, getters } from '@/store/project.module';
import ApiService from '@/services/api.service';

describe('Vuex Project Module', () => {
  describe('Mutations', () => {
    it('it can set a project', () => {
      const state = { project: null };
      const project = { _id: '1' };

      mutations.setProject(state, project);

      assert.equal(state.project, project);
    });

    it("it can set the project's devlog list", () => {
      const state = { devlogs: null };
      const devlogs = [];

      mutations.setDevlogs(state, devlogs);

      assert.equal(state.devlogs, devlogs);
    });

    it('it can set the loading state', () => {
      const state = { loading: false };
      const loading = true;

      mutations.setLoading(state, loading);

      assert.equal(state.loading, loading);
    });
  });

  describe('Actions', () => {
    const projectMock = {
      _id: '0',
    };
    const devlogsMock = [
      { title: 'Devlog 1' },
      { title: 'Devlog 2' },
    ];

    let apiServiceStub;
    function stubApiServiceGet(returnValue) {
      apiServiceStub = sinon.stub(ApiService, 'get')
        .callsFake(async () => returnValue);
    }
    function fakeResponse(status, data, error) {
      return {
        status,
        data: {
          data,
          error,
        },
      };
    }

    afterEach(() => {
      apiServiceStub.restore();
    });

    it('it fetches a project', async () => {
      const commit = sinon.spy();
      stubApiServiceGet(fakeResponse(200, projectMock, false));

      await actions.fetchProject({ commit }, projectMock._id);

      assert.deepEqual(apiServiceStub.args, [
        [`/projects/${projectMock._id}`],
      ]);

      assert.deepEqual(commit.args, [
        ['setProject', projectMock],
      ]);

      assert.isTrue(apiServiceStub.calledBefore(commit));
    });

    it("it doesn't fetch a project when a previous project is given", async () => {
      const commit = sinon.spy();
      stubApiServiceGet(fakeResponse(200, projectMock, false));

      const prevProject = {
        _id: '1',
      };

      await actions.fetchProject({ commit }, prevProject._id, prevProject);

      assert.isFalse(apiServiceStub.called);

      assert.deepEqual(commit.args, [
        ['setProject', prevProject],
      ]);
    });

    it("it fetches the project's devlog list", async () => {
      const commit = sinon.spy();
      stubApiServiceGet(fakeResponse(200, devlogsMock, false));

      await actions.fetchDevlogs({ commit }, { idProject: projectMock._id, owner: false });

      assert.deepEqual(apiServiceStub.args, [
        [`/devlogs?project=${projectMock._id}&published=true`],
      ]);

      assert.deepEqual(commit.args, [
        ['setDevlogs', devlogsMock],
      ]);

      assert.isTrue(apiServiceStub.calledBefore(commit));
    });

    it('it fetches the project and its devlogs while managing the loading state', async () => {
      const stub = sinon.stub();

      await actions.fetch(
        { commit: stub, dispatch: stub },
        { idProject: projectMock._id, owner: false },
      );

      assert.deepEqual(stub.args, [
        ['setLoading', true],
        ['fetchProject', projectMock._id],
        ['fetchDevlogs', { idProject: projectMock._id, owner: false }],
        ['setLoading', false],
      ]);
    });
  });

  describe('Getters', () => {
    it('it can retrieve the project', () => {
      const state = {
        project: { _id: '0' },
      };

      const project = getters.project(state);

      assert.equal(project, state.project);
    });

    it("it can retrieve the project's devlog list", () => {
      const state = {
        devlogs: [{ _id: '0' }],
      };

      const devlogs = getters.devlogs(state);

      assert.deepEqual(devlogs, state.devlogs);
    });

    it('it can retrieve the loading state', () => {
      const state = {
        loading: true,
      };

      const loading = getters.loading(state);

      assert.equal(loading, state.loading);
    });
  });
});
