import { assert } from 'chai';
import sinon from 'sinon';
import { mutations, actions, getters } from '@/store/devlog.module';
import ApiService from '@/services/api.service';

describe('Vuex Devlog Module', () => {
  describe('Mutations', () => {
    it('it can set a devlog', () => {
      const state = { devlog: null };
      const devlog = { _id: '1' };

      mutations.setDevlog(state, devlog);

      assert.equal(state.devlog, devlog);
    });

    it('it can set the loading state', () => {
      const state = { loading: false };
      const loading = true;

      mutations.setLoading(state, loading);

      assert.equal(state.loading, loading);
    });
  });

  describe('Actions', () => {
    const devlogMock = {
      _id: '0',
    };

    let apiServiceStub;
    function stubApiServiceGet(returnValue) {
      apiServiceStub = sinon.stub(ApiService, 'get')
        .callsFake(async () => returnValue);
    }

    function fakeResponse(status, data, error) {
      return {
        status,
        data: {
          data,
          error,
        },
      };
    }

    afterEach(() => {
      apiServiceStub.restore();
    });

    it('it fetches a devlog', async () => {
      const commit = sinon.spy();
      stubApiServiceGet(fakeResponse(200, devlogMock, false));

      await actions.fetchDevlog({ commit }, devlogMock._id);

      assert.deepEqual(apiServiceStub.args, [
        [`/devlogs/${devlogMock._id}`],
      ]);

      assert.deepEqual(commit.args, [
        ['setLoading', true],
        ['setDevlog', devlogMock],
        ['setLoading', false],
      ]);

      assert.isTrue(apiServiceStub.calledBefore(commit));
    });

    it("it doesn't fetch a devlog when a previous devlog is given", async () => {
      const commit = sinon.spy();
      stubApiServiceGet(fakeResponse(200, devlogMock, false));

      const prevDevlog = {
        _id: '1',
      };

      await actions.fetchDevlog({ commit }, prevDevlog._id, prevDevlog);

      assert.isFalse(apiServiceStub.called);

      assert.deepEqual(commit.args, [
        ['setLoading', true],
        ['setDevlog', prevDevlog],
        ['setLoading', false],
      ]);
    });
  });

  describe('Getters', () => {
    it('it can retrieve the devlog', () => {
      const state = {
        devlog: { _id: '0' },
      };

      const devlog = getters.devlog(state);

      assert.equal(devlog, state.devlog);
    });
  });
});
