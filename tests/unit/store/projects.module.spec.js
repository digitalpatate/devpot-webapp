import { assert } from 'chai';
import sinon from 'sinon';
import { mutations, actions, getters } from '@/store/projects.module';
import ApiService from '@/services/api.service';

describe('Vuex Devlog Module', () => {
  describe('Mutations', () => {
    it('it can set the projects list', () => {
      const state = { projects: null };
      const projects = [{ _id: '1' }];

      mutations.setProjects(state, projects);

      assert.equal(state.projects, projects);
    });

    it('it can set the loading state', () => {
      const state = { loading: false };
      const loading = true;

      mutations.setLoading(state, loading);

      assert.equal(state.loading, loading);
    });
  });

  describe('Actions', () => {
    const projectsMock = [{
      _id: '0',
    }];

    let apiServiceStub;
    function stubApiServiceGet(returnValue) {
      apiServiceStub = sinon.stub(ApiService, 'get')
        .callsFake(async () => returnValue);
    }

    function fakeResponse(status, data, error) {
      return {
        status,
        data: {
          data,
          error,
        },
      };
    }

    afterEach(() => {
      apiServiceStub.restore();
    });

    it('it fetches the project list', async () => {
      const commit = sinon.spy();
      stubApiServiceGet(fakeResponse(200, projectsMock, false));

      await actions.fetchProjects({ commit });

      assert.deepEqual(apiServiceStub.args, [
        ['/projects'],
      ]);

      assert.deepEqual(commit.args, [
        ['setLoading', true],
        ['setProjects', projectsMock],
        ['setLoading', false],
      ]);

      assert.isTrue(apiServiceStub.calledBefore(commit));
    });

    it("it doesn't fetch the project when a previous list is given", async () => {
      const commit = sinon.spy();
      stubApiServiceGet(fakeResponse(200, projectsMock, false));

      const prevProjects = [{
        _id: '1',
      }];

      await actions.fetchProjects({ commit }, prevProjects);

      assert.isFalse(apiServiceStub.called);

      assert.deepEqual(commit.args, [
        ['setLoading', true],
        ['setProjects', prevProjects],
        ['setLoading', false],
      ]);
    });
  });

  describe('Getters', () => {
    it('it can retrieve the project list', () => {
      const state = {
        projects: [{ _id: '0' }],
      };

      const projects = getters.projects(state);

      assert.deepEqual(projects, state.projects);
    });

    it('it can retrieve the loading state', () => {
      const state = {
        loading: true,
      };

      const loading = getters.loading(state);

      assert.equal(loading, state.loading);
    });
  });
});
