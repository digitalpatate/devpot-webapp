import { assert } from 'chai';
import sinon from 'sinon';
import { mutations, actions } from '../../../src/store/user.module';
import AuthService from '@/services/auth.service';
import JWTService from '@/services/jwt.service';

describe('Vuex user Module', () => {
  describe('mutations', () => {
    it("it can set the user's informations", async () => {
      const state = { user: null };
      const user = { id: '1' };

      mutations.setUser(state, user);

      assert.equal(state.user, user);
    });
  });

  describe('actions', () => {
    const token = 'a1b2c4d4e5';
    const id = '1';
    const username = 'jack';
    const email = 'jack@potato';
    const password = '123';

    it('it can login', async () => {
      const data = { _id: id, username, email };
      const commit = sinon.spy();
      const loginStub = sinon.stub(AuthService, 'login')
        .returns({ error: false, token, data });
      const storeTokenStub = sinon.stub(JWTService, 'storeToken');
      const storeIdStub = sinon.stub(AuthService, 'storeUserId');

      assert.isTrue(await actions.login({ commit }, { username, password }));

      assert.deepEqual(loginStub.args, [
        [username, password],
      ]);

      assert.isTrue(storeTokenStub.calledOnce);
      assert.isTrue(storeIdStub.calledOnce);
      assert.deepEqual(commit.args, [
        ['setUser', { id, username, email }],
      ]);

      assert.isTrue(loginStub.calledBefore(commit));

      loginStub.restore();
      storeTokenStub.restore();
      storeIdStub.restore();
    });

    it('it handles correctly a failed login', async () => {
      const commit = sinon.spy();
      const loginStub = sinon.stub(AuthService, 'login')
        .returns({ error: true });
      const storeTokenStub = sinon.stub(JWTService, 'storeToken');
      const storeIdStub = sinon.stub(AuthService, 'storeUserId');

      assert.isFalse(await actions.login({ commit }, { username, password }));

      assert.deepEqual(loginStub.args, [
        [username, password],
      ]);

      assert.isTrue(storeTokenStub.notCalled);
      assert.isTrue(storeIdStub.notCalled);
      assert.isTrue(commit.notCalled);

      loginStub.restore();
      storeTokenStub.restore();
      storeIdStub.restore();
    });

    it('it can fetch the connected user informations', async () => {
      const data = { _id: id, username, email };
      const commit = sinon.spy();
      const getUserStub = sinon.stub(AuthService, 'getUser')
        .returns({ error: false, data });
      const getUserIdStub = sinon.stub(AuthService, 'getUserId')
        .returns(id);

      assert.isTrue(await actions.fetchUser({ commit }));

      assert.isTrue(getUserIdStub.calledOnce);
      assert.isTrue(getUserIdStub.calledBefore(getUserStub));

      assert.deepEqual(getUserStub.args, [
        [id],
      ]);

      assert.deepEqual(commit.args, [
        ['setUser', { id, username, email }],
      ]);

      assert.isTrue(getUserStub.calledBefore(commit));

      getUserStub.restore();
      getUserIdStub.restore();
    });

    it('it handles corrently when cannot fetch the user informations', async () => {
      const data = { _id: id, username, email };
      const commit = sinon.spy();
      const getUserStub = sinon.stub(AuthService, 'getUser')
        .returns({ error: true, data });
      const getUserIdStub = sinon.stub(AuthService, 'getUserId')
        .returns(id);

      assert.isFalse(await actions.fetchUser({ commit }));

      assert.isTrue(getUserIdStub.calledOnce);
      assert.isTrue(getUserIdStub.calledBefore(getUserStub));

      assert.deepEqual(getUserStub.args, [
        [id],
      ]);

      assert.deepEqual(commit.args, [
        ['setUser', null],
      ]);

      getUserStub.restore();
      getUserIdStub.restore();
    });

    it('it can check auth and fetch the user if connected', async () => {
      const getters = { isAuthenticated: true };
      const dispatch = sinon.spy();
      const getTokenStub = sinon.stub(JWTService, 'getToken').returns(true);
      const getIdStub = sinon.stub(AuthService, 'getUserId').returns(true);

      await actions.checkAuth({ dispatch, getters });

      assert.deepEqual(dispatch.args, [
        ['fetchUser'],
        ['fetchAccount'],
      ]);

      getTokenStub.restore();
      getIdStub.restore();
    });

    it('it can check auth an reset user if not connected', async () => {
      const dispatch = sinon.spy();
      const getTokenStub = sinon.stub(JWTService, 'getToken').returns(false);
      const getIdStub = sinon.stub(AuthService, 'getUserId').returns(false);

      await actions.checkAuth({ dispatch });

      assert.deepEqual(dispatch.args, [
        ['clearAuth'],
      ]);

      getTokenStub.restore();
      getIdStub.restore();
    });

    it('it can clear auth data', async () => {
      const commit = sinon.spy();
      const dispatch = sinon.spy();
      const removeTokenStub = sinon.stub(JWTService, 'removeToken');
      const removeIdStub = sinon.stub(AuthService, 'removeUserId');

      await actions.clearAuth({ commit, dispatch });

      assert.isTrue(removeTokenStub.calledOnce);
      assert.isTrue(removeIdStub.calledOnce);

      assert.deepEqual(commit.args, [
        ['setUser', null],
        ['setAccount', null],
      ]);

      removeTokenStub.restore();
      removeIdStub.restore();
    });
  });
});
