FROM node:latest

RUN mkdir -p /usr/src/app/tmp
WORKDIR /usr/src/app/tmp
COPY . ./

RUN npm install
RUN npm run build

WORKDIR /usr/src/app
RUN mv ./tmp/server/* ./
RUN rm -r ./tmp
RUN npm install

EXPOSE 8080

CMD ["npm", "start"]